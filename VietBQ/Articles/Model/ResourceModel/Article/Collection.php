<?php
namespace VietBQ\Articles\Model\ResourceModel\Article;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'article_id';
    protected $_eventPrefix = 'vietbq_articles_article_collection';
    protected $_eventObject = 'article_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('VietBQ\Articles\Model\Article', 'VietBQ\Articles\Model\ResourceModel\Article');
    }

}