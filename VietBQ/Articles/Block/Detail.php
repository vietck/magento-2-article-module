<?php

namespace VietBQ\Articles\Block;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use VietBQ\Articles\Model\ArticleFactory;
use VietBQ\Articles\Controller\Index\Detail as ViewAction;
use Magento\Framework\UrlInterface;

class Detail extends Template
{
    protected $_articleFactory;
    protected $_article = null;
    protected $_registry;
    protected $_urlInterface;

    public function __construct(Context $context,
                                ArticleFactory $articleFactory,
                                Registry $registry,
                                UrlInterface $urlInterface)
    {
        $this->_articleFactory = $articleFactory;
        $this->_registry = $registry;
        $this->_urlInterface = $urlInterface;
        parent::__construct($context);
    }

    public function getArticle()
    {
        if ($this->_article === null) {
            $article = $this->_articleFactory->create();
            $article->load($this->_getArticleID());

            if (!$article->getArticleId()) {
                throw new LocalizedException("Article not found!");
            }

            $this->_article = $article;
        }

        return $this->_article;
    }

    private function _getArticleID()
    {
        return (int) $this->_registry->registry(
            ViewAction::REGISTRY_KEY_ARTICLE_ID
        );
    }

    public function getAllArticlesURL()
    {
        return $this->_urlInterface->getUrl('articles');
    }
}